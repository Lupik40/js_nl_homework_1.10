//PS: Стили всех элементов остаются на ваш вкус и цвет

/* 
    TASK 1

    На странице html у нас имеется разметка:

    <header>
      <nav>
        <ul>
          <li>
            <a>Home</a>
          </li>
          <li>
            <a>News</a>
          </li>
          <li>
            <a>Contacts</a>
          </li>
        </ul>
      </nav>
    </header>

    Необходимо добавить с ПОМОЩЬЮ JAVASCRIPT в навигацию по сайту следующие разделы :

    - About Us
    - Our Team

    Первый раздел должен находится перед News

    Второй перед Contacts

    Единственные правки в html которые вы можете внести, это добавить атрибуты элементам

*/

const menu1 = document.querySelector('.menu-1'),
      menu5 = document.querySelector('.menu-5');

const menu2 = document.createElement('li'),
      menu4 = document.createElement('li');
      
const a2 = document.createElement('a');
const a4 = document.createElement('a');

a2.innerHTML = 'About Us';
a4.innerHTML = 'Our Team';

menu2.append(a2);
menu4.append(a4);

menu1.after(menu2);
menu5.before(menu4);

/* 
    TASK 2
    На странице html у нас имеется разметка:
    <header>
      <nav>
        <ul>
          <li>
            <a>Home</a>
          </li>
          <li>
            <a>News</a>
          </li>
          <li>
            <a>Contacts</a>
          </li>
        </ul>
      </nav>
    </header>
  Необходимо ПОЛНОСТЬЮ скопировать навигацию из шапки в футер, при этом не касаясь файла html.

  outcome of task :

  Две одинаковых навигации и в шапке и в футере
*/


const header = document.querySelector('header');
const footer = document.createElement('footer');
const nav = document.querySelector('.nav');

let newFooter = nav.cloneNode(true);

header.after(footer);
footer.append(newFooter);



/* 
    TASK 3

    На странице HTML мы имеем:

    <button>Open the first block</button>

    <button>Open the second block</button>

    <button>Open the third block</button>

    <div class="block">First Block</div>

    <div class="block">Second Block</div>

    <div class="block">Third Block</div>

    все элементы с классом "block" по умолчанию скрыты

    Задача:

    - при нажатии на первую кнопку пользователь может открывать/закрывать First Block

    - при нажатии на вторую кнопку пользователь может открывать/закрывать Second Block

    - при нажатии на третью кнопку пользователь может открывать/закрывать Third Block
    
*/

const btns = document.querySelectorAll('.btn');
const blocks = document.querySelectorAll('.block');

for(let i = 0; i < btns.length; i++) {
  btns[i].addEventListener('click', function() {
    blocks[i].classList.toggle('active');
  })};


/* 
    TASK 4

    Имеем блок 300px x 300px, внутри него много текста который не помещается в него

    Соответсвенно у этого блока добавляем стиль overflow:scroll

    В результате мы должны получить блок с полосой прокрутки и возможностью увидеть весь текст когда мы скроллим блок

    У нас есть рядом с блоком с текстом кнопка, при нажатии на которую мы всегда отправляемся в начало нашего текста

    Эта кнопка по умолчанию скрыта, и появляется только тогда когда пользователь проскролит до 100px

    Когда пользователь оказывается вверху блока(в начале текста), кнопка снова должна скрываться
    
*/

const scrollBox = document.getElementById('box'),
      scrollBtn = document.getElementById('btn-scroll');

scrollBox.addEventListener('scroll', function() {
  if(scrollBox.scrollTop > 200) {
    scrollBtn.classList.add('active');
  } else {
    scrollBtn.classList.remove('active');
  };
});

scrollBtn.addEventListener('click', function() {
  scrollBox.scrollTop = 0;
});

/* 
    TASK 5

    У нас есть html форма:

    <form action="">

      <label for="">FirstName

        <input type="text">

      </label>

      <label for="">LastName

        <input type="text">

      </label>

      <label for="">Phone Number

        <input type="text">

      </label>

      <button>Send the form</button>

    </form>

    1. При вводе имени "Sergey" в input отвечающий за имя, пользователь должен видеть сообщение в alert окне: "Имя как у нашего преподователя"

    2. При вводе фамилии "Eremenko" в input отвечающий за фамилию, пользователь должен видеть сообщение в alert окне: "Фамилия как у нашего наставника"

    3. Создайте пустой div на странице, и при отправке формы данные из трех input должны выводиться в этом самом div, в формате:

    Спасибо за отправку формы, ваши данные:

    FirstName : 'значение из input отвечающий за FirstName'

    LastName : значение из input отвечающий за LastName

    PhoneNumber : значение из input отвечающий за PhoneNumber

    Перед отправкой формы этот div должен быть пустым

*/

const firstNameInp = document.getElementById('first-name'),
      lastNameInp = document.getElementById('last-name'),
      phoneInp = document.getElementById('phone-form'),
      formSend = document.getElementById('form');


const formResult = document.getElementById('form-result');

firstNameInp.addEventListener('input', function() {
  if(firstNameInp.value == 'Sergey') {
    alert("Имя как у нашего преподователя")
  };
});

lastNameInp.addEventListener('input', function() {
  if(lastNameInp.value == 'Eremenko') {
    alert("Фамилия как у нашего наставника");
  };
});

form.addEventListener('submit', function(e) {
    e.preventDefault();
    let resultText = document.createElement('p');

    resultText.textContent = `
        Спасибо за отправку формы, ваши данные:\n
        
        FirstName : ${firstNameInp.value}\n

        LastName : ${lastNameInp.value}\n

        PhoneHumber : ${phoneInp.value}
    `;

    formResult.append(resultText);
});
 






